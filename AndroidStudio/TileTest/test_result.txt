.FFF
======================================================================
FAIL: test_more_realistic_example__0_00__no_broken_beacons (__main__.test_positions)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "<string>", line 11, in test_more_realistic_example__0_00__no_broken_beacons
  File "test_beacon_reader.py", line 109, in get_positions
    self.devices.items()))
AssertionError: 
  distance: 0.590953
  expected: < 0.200000
  testing ( 2.50,  3.00,  0.00)
  measured ( 3.00,  3.00,  0.32)
  beacon information:
    mac 00:07:80:68:28:67, pos ( 3.00,  3.00,  0.50), rssi 4.01, meas dist 0.71, act dist 0.71, meas dist error -0.00
    mac 00:07:80:c0:ff:ee, pos ( 0.50,  5.00,  1.00), rssi -8.54, meas dist 3.00, act dist 3.00, meas dist error -0.00
    mac 00:07:80:7e:c3:68, pos ( 1.00,  0.50,  0.50), rssi -8.42, meas dist 2.96, act dist 2.96, meas dist error -0.00
    mac 00:07:80:7e:c3:7b, pos ( 5.00,  2.00,  1.00), rssi -8.16, meas dist 2.87, act dist 2.87, meas dist error -0.00
    mac 00:07:80:52:64:e6, pos ( 2.50,  5.00,  1.00), rssi -5.99, meas dist 2.24, act dist 2.24, meas dist error -0.00
    mac 00:07:80:79:1f:f1, pos ( 6.00,  0.50,  1.80), rssi -12.37, meas dist 4.66, act dist 4.66, meas dist error -0.00
    mac 00:07:80:68:1c:9c, pos ( 0.50,  0.50,  2.20), rssi -10.79, meas dist 3.88, act dist 3.88, meas dist error -0.00
    mac 00:07:80:68:28:29, pos ( 6.00,  5.00,  1.00), rssi -11.37, meas dist 4.15, act dist 4.15, meas dist error -0.00

======================================================================
FAIL: test_simple_example_1__0_00__no_broken_beacons (__main__.test_positions)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "<string>", line 11, in test_simple_example_1__0_00__no_broken_beacons
  File "test_beacon_reader.py", line 109, in get_positions
    self.devices.items()))
AssertionError: 
  distance: 0.232594
  expected: < 0.200000
  testing ( 0.10,  0.00,  0.00)
  measured ( 0.00,  0.00, -0.21)
  beacon information:
    mac 00:07:80:7e:c3:68, pos ( 2.00,  0.00,  0.00), rssi -4.58, meas dist 1.90, act dist 1.90, meas dist error -0.00
    mac 00:07:80:68:1c:9c, pos ( 0.00,  0.00,  1.00), rssi 0.96, meas dist 1.00, act dist 1.00, meas dist error -0.00
    mac 00:07:80:7e:c3:7b, pos ( 0.00,  2.00,  0.00), rssi -5.03, meas dist 2.00, act dist 2.00, meas dist error -0.00
    mac 00:07:80:52:64:e6, pos ( 0.00,  0.00,  0.00), rssi 21.00, meas dist 0.10, act dist 0.10, meas dist error -0.00

======================================================================
FAIL: test_simple_example_2__0_00__no_broken_beacons (__main__.test_positions)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "<string>", line 11, in test_simple_example_2__0_00__no_broken_beacons
  File "test_beacon_reader.py", line 109, in get_positions
    self.devices.items()))
AssertionError: 
  distance: 0.232594
  expected: < 0.200000
  testing ( 0.10,  0.00,  0.00)
  measured ( 0.00,  0.00, -0.21)
  beacon information:
    mac 00:07:80:7e:c3:68, pos ( 2.00,  2.00,  0.00), rssi -7.81, meas dist 2.76, act dist 2.76, meas dist error -0.00
    mac 00:07:80:68:1c:9c, pos ( 0.00,  2.00,  1.00), rssi -6.00, meas dist 2.24, act dist 2.24, meas dist error -0.00
    mac 00:07:80:7e:c3:7b, pos ( 2.00,  0.00,  1.00), rssi -5.64, meas dist 2.15, act dist 2.15, meas dist error -0.00
    mac 00:07:80:52:64:e6, pos ( 0.00,  0.00,  0.00), rssi 21.00, meas dist 0.10, act dist 0.10, meas dist error -0.00

----------------------------------------------------------------------
Ran 4 tests in 0.019s

FAILED (failures=3)
##### sx  ####
[3.0, 3.0, 0.3]
### r ###
array([ 0.2       ,  3.27719392,  3.20780299,  2.3430749 ,  2.17715411,
        4.18330013,  4.01372645,  3.67287353])
### r ###
array([ 0.25      ,  3.39300752,  3.30340733,  2.21641603,  2.21641603,
        4.07707003,  4.10883195,  3.55140817])
### r ###
array([ 0.25      ,  3.18786763,  3.32603367,  2.41091269,  2.04022058,
        4.27463449,  4.10883195,  3.59339672])
### r ###
array([ 0.185     ,  3.27402275,  3.20690271,  2.33863742,  2.17237773,
        4.17794507,  4.0066476 ,  3.67004428])

###########################################
  distance: 0.590953
  expected: < 0.200000
  testing ( 2.50,  3.00,  0.00)
  measured ( 3.00,  3.00,  0.32)
  beacon information:
    mac 00:07:80:68:28:67, pos ( 3.00,  3.00,  0.50), rssi 4.01, meas dist 0.71, act dist 0.71, meas dist error -0.00
    mac 00:07:80:c0:ff:ee, pos ( 0.50,  5.00,  1.00), rssi -8.54, meas dist 3.00, act dist 3.00, meas dist error -0.00
    mac 00:07:80:7e:c3:68, pos ( 1.00,  0.50,  0.50), rssi -8.42, meas dist 2.96, act dist 2.96, meas dist error -0.00
    mac 00:07:80:7e:c3:7b, pos ( 5.00,  2.00,  1.00), rssi -8.16, meas dist 2.87, act dist 2.87, meas dist error -0.00
    mac 00:07:80:52:64:e6, pos ( 2.50,  5.00,  1.00), rssi -5.99, meas dist 2.24, act dist 2.24, meas dist error -0.00
    mac 00:07:80:79:1f:f1, pos ( 6.00,  0.50,  1.80), rssi -12.37, meas dist 4.66, act dist 4.66, meas dist error -0.00
    mac 00:07:80:68:1c:9c, pos ( 0.50,  0.50,  2.20), rssi -10.79, meas dist 3.88, act dist 3.88, meas dist error -0.00
    mac 00:07:80:68:28:29, pos ( 6.00,  5.00,  1.00), rssi -11.37, meas dist 4.15, act dist 4.15, meas dist error -0.00
##### sx  ####
[0.0, 0.0, -0.2]
### r ###
array([ 2.00997512,  1.2       ,  2.00997512,  0.2       ])
### r ###
array([ 2.00972637,  1.20000003,  2.00997514,  0.20000016])
### r ###
array([ 2.00997514,  1.20000003,  2.00972637,  0.20000016])
### r ###
array([ 2.01099478,  1.21      ,  2.01099478,  0.21      ])

###########################################
  distance: 0.232594
  expected: < 0.200000
  testing ( 0.10,  0.00,  0.00)
  measured ( 0.00,  0.00, -0.21)
  beacon information:
    mac 00:07:80:7e:c3:68, pos ( 2.00,  0.00,  0.00), rssi -4.58, meas dist 1.90, act dist 1.90, meas dist error -0.00
    mac 00:07:80:68:1c:9c, pos ( 0.00,  0.00,  1.00), rssi 0.96, meas dist 1.00, act dist 1.00, meas dist error -0.00
    mac 00:07:80:7e:c3:7b, pos ( 0.00,  2.00,  0.00), rssi -5.03, meas dist 2.00, act dist 2.00, meas dist error -0.00
    mac 00:07:80:52:64:e6, pos ( 0.00,  0.00,  0.00), rssi 21.00, meas dist 0.10, act dist 0.10, meas dist error -0.00
##### sx  ####
[0.0, 0.0, -0.2]
### r ###
array([ 2.83548938,  2.33238076,  2.33238076,  0.2       ])
### r ###
array([ 2.83531304,  2.33238077,  2.33216639,  0.20000016])
### r ###
array([ 2.83531304,  2.33216639,  2.33238077,  0.20000016])
### r ###
array([ 2.83621226,  2.33754144,  2.33754144,  0.21      ])

###########################################
  distance: 0.232594
  expected: < 0.200000
  testing ( 0.10,  0.00,  0.00)
  measured ( 0.00,  0.00, -0.21)
  beacon information:
    mac 00:07:80:7e:c3:68, pos ( 2.00,  2.00,  0.00), rssi -7.81, meas dist 2.76, act dist 2.76, meas dist error -0.00
    mac 00:07:80:68:1c:9c, pos ( 0.00,  2.00,  1.00), rssi -6.00, meas dist 2.24, act dist 2.24, meas dist error -0.00
    mac 00:07:80:7e:c3:7b, pos ( 2.00,  0.00,  1.00), rssi -5.64, meas dist 2.15, act dist 2.15, meas dist error -0.00
    mac 00:07:80:52:64:e6, pos ( 0.00,  0.00,  0.00), rssi 21.00, meas dist 0.10, act dist 0.10, meas dist error -0.00
