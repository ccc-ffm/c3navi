package so.libcrypt.tiletest;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.qozix.tileview.TileView;


public class MyActivity extends TileViewActivity {
    private ImageView marker;
    private TileView tileView;
    final int phoehe = 3253;
    final int pbreite = 2504;

    // Aktuelles Level
    private int cur_lvl;
    // Element 0 == sublevel0-Item; Element 1 == sublevel1-Item etc...
    private final int[] lvls = {R.id.sublevel0, R.id.sublevel1, R.id.sublevel2, R.id.sublevel3, R.id.sublevel4};

    @Override
    public void onPause() {
        super.onPause();

        // Speichern einiger Eigenschaften
        SharedPreferences.Editor editor = getSharedPreferences("App", 0).edit();
        editor.putInt("lvl", cur_lvl);
        editor.commit();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Erst Activity laden
        setContentView(R.layout.activity_my);
        // Dann den Bereich suchen, wo die Karte sein soll
        ViewGroup map = (ViewGroup) findViewById(R.id.map);

        // Lese "lvl"-Eigenschaft, wenn nicht existent gebe 0 (int) zurueck
        SharedPreferences settings = getSharedPreferences("App", 0);
        cur_lvl = settings.getInt("lvl", 0);

        // View des Kartenmaterials erstellen
        tileView = new TileView(this);
        tileView.setSize(phoehe,pbreite);
        changeLevel(cur_lvl);

        // Das Kartenmaterial im Bereich "map" anzeigen
        map.addView(tileView);

        // Marker erstellen und Icon setzen
        marker = new ImageView(this);
        marker.setImageResource(R.drawable.aim);

        // Mitte des Markers als zentralen Punkt setzen
        tileView.setMarkerAnchorPoints(-0.5f, -0.5f);
        // Marker setzen
        tileView.addMarker(marker, 0, 0);

        tileView.setScale(0);
        tileView.setScaleLimits(1, 2);
        tileView.slideToAndCenter(phoehe / 2, pbreite / 2);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Alle wieder sichtbar machen
        menu.findItem(lvls[0]).setVisible(true);
        menu.findItem(lvls[1]).setVisible(true);
        menu.findItem(lvls[2]).setVisible(true);
        menu.findItem(lvls[3]).setVisible(true);
        menu.findItem(lvls[4]).setVisible(true);

        // Die Auswahl der vorhanden Level einschraenken. Das aktuelle Level ist
        // nicht erforderlich.
        menu.findItem(lvls[cur_lvl]).setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            // Setting is called
            case R.id.action_settings:
                return true;
            // Move Marker
            case R.id.marker1:
                tileView.moveMarker(marker, 1626,1252);
                break;
            // Move Marker to another position
            case R.id.marker2:
                tileView.moveMarker(marker, 3000, 1800);
                break;

            // Below: change lvls
            case R.id.sublevel0:
                changeLevel(0);
                break;
            case R.id.sublevel1:
                changeLevel(1);
                break;
            case R.id.sublevel2:
                changeLevel(2);
                break;
            case R.id.sublevel3:
                changeLevel(3);
                break;
            case R.id.sublevel4:
                changeLevel(4);
                break;

            default: return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //change lvl by reset, add new lvl and refresh the view
    private void changeLevel(int lvl) {
        cur_lvl = lvl;
        tileView.resetDetailLevels();
        tileView.addDetailLevel(1f, "tiles/30c3/"+ lvl +"/tile-%col%_%row%.png");
        tileView.refresh();

        //Update lvl-Anzeige
        TextView lvlView = (TextView) findViewById(R.id.lvl);
        lvlView.setText("lvl "+ lvl);

        doToast("You'r now on Level " + lvl);
    }


    private void doToast(CharSequence txt) {
        Toast.makeText(getApplicationContext(), txt, Toast.LENGTH_SHORT).show();
    }
}
